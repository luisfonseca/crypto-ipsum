from random import randint
from flask import Flask, request, render_template

from sample import sample

app = Flask(__name__)

class Args:
  def __init__(self,n):
    n = n if n is not None else 200
    self.n = int(float(n)) if int(float(n)) <= 1000 else 200
  pick=1
  count = 1
  prime = ' '
  quiet = True
  sample = 2
  save_dir = 'save'
  width = 4

@app.route("/")
def index():
    args = Args(request.args.get('n'))
    sampleText = sample(args)
    jargon = ''

    for sentence in sampleText.split('.'):
        if randint(1,5) is 5:
            jargon += sentence + '.<br />'
        else:
            jargon += sentence + '.'

    return render_template('index.html',jargon=jargon, initN=args.n)

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=80)
